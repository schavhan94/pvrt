from django.conf import settings
from log_request_id.middleware import (RequestIDMiddleware, REQUEST_ID_HEADER_SETTING, DEFAULT_NO_REQUEST_ID,
                             GENERATE_REQUEST_ID_IF_NOT_IN_HEADER_SETTING, LOG_REQUESTS_NO_SETTING)



def monkey_patch_get_request_id(self, request):
    ''''''
    request_id_header = getattr(settings, REQUEST_ID_HEADER_SETTING, None)
    correlation_id_header = getattr(settings,'CORR_ID', None)
    generate_request_if_not_in_header = getattr(settings, GENERATE_REQUEST_ID_IF_NOT_IN_HEADER_SETTING, False)
    
    if request_id_header:

        default_request_id = getattr(settings, LOG_REQUESTS_NO_SETTING, DEFAULT_NO_REQUEST_ID)
        
        if correlation_id_header:
            corr_id = request.META.get(correlation_id_header,None)
            if corr_id:
                default_request_id = corr_id
            elif generate_request_if_not_in_header:
                default_request_id = self._generate_id()
            
        return request.META.get(request_id_header, default_request_id)

    return self._generate_id()

RequestIDMiddleware._get_request_id = monkey_patch_get_request_id