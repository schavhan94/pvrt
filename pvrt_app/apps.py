from django.apps import AppConfig


class PvrtAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'pvrt_app'
